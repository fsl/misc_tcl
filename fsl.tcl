#   fsl - meta-menu for fsl tools
#
#   Stephen Smith, FMRIB Image Analysis Group
#
#   Copyright (C) 1999-2003 University of Oxford
#
#   TCLCOPYRIGHT

#{{{ setups

source [ file dirname [ info script ] ]/fslstart.tcl

set VARS(history) {}

#}}}

proc fsl { w } {

    #{{{ vars and setup

global FSLDIR argc argv PWD HOME gui_ext

set FSLVERSION ""

if { [ file exists ${FSLDIR}/etc/fslversion ] } {
    set FSLVERSION [ exec sh -c "cat ${FSLDIR}/etc/fslversion" ]
}

toplevel $w -menu $w.f1.fmrib.menu
wm title $w "FSL $FSLVERSION"
wm iconname $w "FSL"
wm iconbitmap $w @${FSLDIR}/tcl/fmrib.xbm

set graphpic [image create photo -file ${FSLDIR}/doc/images/fsl-logo.gif ]

frame $w.f1

button $w.f1.label -image $graphpic -borderwidth 0 -relief raised -borderwidth 2 -command "FmribWebHelp https: fsl.fmrib.ox.ac.uk/fsl/fslwiki/"

pack $w.f1.label -in $w.f1

#}}}
    #{{{ main menu

if { [ file exists ${FSLDIR}/tcl/loadvarian.tcl ] } {
    button $w.f1.loadvarian -text "Load Varian" \
	    -command { exec sh -c "${FSLDIR}/bin/LoadVarian$gui_ext" & }
    pack $w.f1.loadvarian -in $w.f1 -fill x -padx 1 -pady 1
}

if { [ file exists ${FSLDIR}/tcl/bet.tcl ] } {
    button $w.f1.bet -text "BET brain extraction" \
	    -command { exec sh -c "${FSLDIR}/bin/Bet$gui_ext" & }
    pack $w.f1.bet -in $w.f1 -fill x -padx 1 -pady 1
}

if { [ file exists ${FSLDIR}/tcl/susan.tcl ] } {
    button $w.f1.susan -text "SUSAN noise reduction" \
	    -command { exec sh -c "${FSLDIR}/bin/Susan$gui_ext" & }
    pack $w.f1.susan -in $w.f1 -fill x -padx 1 -pady 1
}

if { [ file exists ${FSLDIR}/tcl/fast.tcl ] } {
    button $w.f1.fast -text "FAST Segmentation" \
	    -command { exec sh -c "${FSLDIR}/bin/Fast$gui_ext" & }
    pack $w.f1.fast -in $w.f1 -fill x -padx 1 -pady 1
}

if { [ file exists ${FSLDIR}/tcl/flirt.tcl ] } {
    button $w.f1.flirt -text "FLIRT linear registration" \
	    -command { exec sh -c "${FSLDIR}/bin/Flirt$gui_ext" & }
    pack $w.f1.flirt -in $w.f1 -fill x -padx 1 -pady 1
}

if { [ file exists ${FSLDIR}/tcl/feat.tcl ] } {
    button $w.f1.feat -text "FEAT FMRI analysis" \
	    -command { exec sh -c "${FSLDIR}/bin/Feat$gui_ext" & }
    pack $w.f1.feat -in $w.f1 -fill x -padx 1 -pady 1
}

if { [ file exists ${FSLDIR}/tcl/melodic.tcl ] } {
    button $w.f1.melodic -text "MELODIC ICA" \
	    -command { exec sh -c "${FSLDIR}/bin/Melodic$gui_ext" & }
    pack $w.f1.melodic -in $w.f1 -fill x -padx 1 -pady 1
}

if { [ file exists ${FSLDIR}/tcl/fdt.tcl ] } {
    button $w.f1.fdt -text "FDT diffusion" \
	    -command { exec sh -c "${FSLDIR}/bin/Fdt$gui_ext" & }
    pack $w.f1.fdt -in $w.f1 -fill x -padx 1 -pady 1
}

if { [ file exists ${FSLDIR}/tcl/possum.tcl ] } {
    button $w.f1.possum -text "POSSUM MRI simulator" \
	    -command { exec sh -c "${FSLDIR}/bin/Possum$gui_ext" & }
    pack $w.f1.possum -in $w.f1 -fill x -padx 1 -pady 1
}

if { [ file exists ${FSLDIR}/bin/fsleyes ] } {
    button $w.f1.fslview -text "FSLeyes" \
	    -command { exec sh -c "${FSLDIR}/bin/fsleyes" & }
    pack $w.f1.fslview -in $w.f1 -fill x -padx 1 -pady 1
}

#}}}
    #{{{ Button Frame

    frame $w.btns

    #{{{ misc menu

menubutton $w.menub -text "Misc" -menu $w.menub.menu -relief raised -bd 2

menu $w.menub.menu

if { [ file exists ${FSLDIR}/tcl/applyxfm.tcl ] } {
    $w.menub.menu add command -label "Apply FLIRT transform" -command { exec sh -c "${FSLDIR}/bin/ApplyXFM$gui_ext" & }
}

if { [ file exists ${FSLDIR}/tcl/concatxfm.tcl ] } {
    $w.menub.menu add command -label "Concat FLIRT transforms" -command { exec sh -c "${FSLDIR}/bin/ConcatXFM$gui_ext" & }
}

if { [ file exists ${FSLDIR}/tcl/invertxfm.tcl ] } {
    $w.menub.menu add command -label "Invert FLIRT transform" -command { exec sh -c "${FSLDIR}/bin/InvertXFM$gui_ext" & }
}

if { [ file exists ${FSLDIR}/tcl/glm.tcl ] } {
    $w.menub.menu add command -label "GLM Setup" -command { exec sh -c "${FSLDIR}/bin/Glm$gui_ext" & }
}

if { [ file exists ${FSLDIR}/tcl/make_flobs.tcl ] } {
    $w.menub.menu add command -label "Make FLOBS" -command { exec sh -c "${FSLDIR}/bin/Make_flobs$gui_ext" & }
}

if { [ file exists ${FSLDIR}/tcl/featquery.tcl ] } {
    $w.menub.menu add command -label "Featquery" -command { exec sh -c "${FSLDIR}/bin/Featquery$gui_ext" & }
}

if { [ file exists ${FSLDIR}/tcl/renderhighres.tcl ] } {
    $w.menub.menu add command -label "Renderhighres" -command { exec sh -c "${FSLDIR}/bin/Renderhighres$gui_ext" & }
}

#}}}

    button $w.cancel -command "destroy $w" -text "Exit"

    button $w.help -command  "FmribWebHelp https: fsl.fmrib.ox.ac.uk/fsl/fslwiki/" -text "Help"

    pack $w.menub $w.cancel $w.help -in $w.btns -side left -anchor s -expand yes -padx 2 -pady 2 -fill x

    pack $w.f1 $w.btns -expand yes -fill x

#}}}
}

wm withdraw .
fsl .rename
tkwait window .rename
