#

#   fsl:exec execute a shell command with logging all setup
#
#   Stephen Smith, FMRIB Image Analysis Group
#
#   Copyright (C) 2000-2007 University of Oxford
#
#   TCLCOPYRIGHT

#
# usage: fsl:echo <filename> "string" [-o]
# -o : overwrite instead of appending
#
proc fsl:echo { thefilename thestring args } {
    if { $thefilename != "" } {
	if { [ llength $args ] != 0 } {
	    catch { exec sh -c "echo \"$thestring\" >  $thefilename" >& /dev/null } putserr
	} else {
	    catch { exec sh -c "echo \"$thestring\" >> $thefilename" >& /dev/null } putserr
	}
    } else {
	catch { puts $thestring } putserr
    }
    if { [ glob -nocomplain logs/* ] != "" } {
	catch { exec sh -c "cat logs/* > report_log.html" } putserr
    }
}


# usage: fsl:exec "the command" [options]
# note the double-quotes round "the command"
# -n                : don't output logging information
# -b <job_duration> : submit to fsl_sub (if available) and return immediately; otherwise wait to return until <command> has finished
#                     job_duration is in minutes, set 0 for unknown
# -f                : "the command" is a filename to be treated as a parallel batch job (one command per line)
# -h <job-hold-id>  : ID of fsl_sub job that must finish before this job gets run
# -N <job_name>     : the job will have the name <job_name> on the fsl_sub submission
# -l <logdir>       : output logs in <logdir>
# -i                : ignore errors ( return exit status 0 to calling script )

proc fsl:exec { thecommand args } {

    global env logout comout FSLDIR FD SGEID

    # process args
    set do_logout 1
    set do_runfsl 0
    set do_runfsl_command ""
    set do_parallel ""
    set job_holds ""
    set job_name ""
    set runtime 300
    set ignoreErrors 0
    set args [ join $args ]
    set logdir ""
    for { set argindex 0 } { $argindex < [ llength $args ] } { incr argindex 1 } {
	set thearg [ lindex $args $argindex ]

	if { $thearg == "-n" } {
	    set do_logout 0
	}

	if { $thearg == "-f" } {
	    set do_parallel "-t"
	}

	if { $thearg == "-h" } {
	    incr argindex 1
	    set theid [ lindex $args $argindex ]
	    if { $theid > 0 } {
		if { $job_holds == "" } {
		    set job_holds "-j $theid"
		}  else {
		    set job_holds "${job_holds},$theid"
		}
	    }
	}

	if { $thearg == "-N" } {
	    incr argindex 1
	    set job_name "-N [ lindex $args $argindex ]"
	}

	if { $thearg == "-l" } {
	    incr argindex 1
	    set logdir "-l [ lindex $args $argindex ]"
	}

	if { $thearg == "-b" } {
	    set do_runfsl 1
	    incr argindex 1
	    set runtime [ lindex $args $argindex ]

 		if { [info exists env(FSL_QUEUE_TIME_SCALE) ] } {
       		set runtime [ expr int(ceil($runtime * $env(FSL_QUEUE_TIME_SCALE))) ]
 		}
	}

	if { $thearg == "-i" } {
	    set ignoreErrors 1
	}


    }

    # add runfsl call if required
    if { $do_runfsl } {
	set thecommand "${FSLDIR}/bin/fsl_sub -T $runtime $logdir $job_name $job_holds $do_parallel $thecommand"
    }

    # save just the command to report.com if it has been setup
    if { [ info exists comout ] && $comout != "" } {
	fsl:echo $comout "$thecommand\n"
    }

    # if logout doesn't exist, set empty so that logging will go to screen
    if { ! [ info exists logout ] } {
	set logout ""
    }

    # run and log the actual command
	if { ! [ info exists ::errorCode ] } { set ::errorCode "NONE" }
    if { $do_logout } {
	fsl:echo $logout "\n$thecommand"
	if { $logout != "" } {
	    set errorCode [ catch { exec sh -c $thecommand >>& $logout } errmsg ]
	    set actualCode [lindex $::errorCode 0]
	    if { $errorCode != 0 && $actualCode ne "NONE" &&  $ignoreErrors != 1 } {
	    } else {
	    # Trim errmsg to final line
		set errmsg [ exec sh -c "tail -n 1 $logout" ]
	    }
	} else {
	    set errorCode [ catch { exec sh -c $thecommand } errmsg ]
	    set actualCode [lindex $::errorCode 0]
	    catch { puts $errmsg } putserr
	}
    } else {
	set errorCode [ catch { exec sh -c $thecommand } errmsg ]
	set actualCode [lindex $::errorCode 0]
    }

    if { $errorCode != 0 && $actualCode ne "NONE" &&  $ignoreErrors != 1 } {
	if { $do_logout } {
	    fsl:echo $logout "\nFATAL ERROR ENCOUNTERED:\nCOMMAND:\n$thecommand\nERROR MESSAGE:\n$errmsg\nEND OF ERROR MESSAGE"
	}
    } else {
       	set errorCode 0
    }


    # now return errmsg in case the exec call needs to know the output
    return -code $errorCode $errmsg
}
